# -*- coding: utf-8 -*-
from app import app
from flask import render_template, url_for, request

@app.route('/')
@app.route('/index')
def index():
    #в данный момент приложение ещё не имеет концепции пользователей,
    #а мы хотим, чтобы наше приложение привествовало пользователей
    #в таких ситуациях используются вымышленные (mock) пользователи.

    #заведём словарь для задания пользователя
    user = {'username': 'инкогнито'}
    #справка о словарях в python
    #https://tproger.ru/explain/python-dictionaries/

    #заведём список словарей - для задания постов о других пользователях:
    #Списки в Python - упорядоченные изменяемые коллекции объектов произвольных типов
    #(почти как массив). https://tproger.ru/translations/python-data-types/
    posts = [
        {
            'author': {'username': 'Alexander'},
            'body': 'Коллеги, с днём 8-го Марта!'
        },
        {
            'author': {'username': 'Denis'},
            'body': 'С чудесным праздником весны!'
        },
        {
            'author': {'username': 'Kateryna'},
            'body': 'Желаю замечательного настроения и много-много улыбок!'
        }
    ]
    #return "Hello, World!"
    return render_template("index.html", title='Главная страницы', user=user, posts=posts)


@app.route('/hello', methods=['GET', 'POST'])
def hello():
    user = request.form['user_name']
    print(user,type(user))
    posts = [
        {
            'author': {'username': 'Kateryna'},
            'body': 'Желаю замечательного настроения и много-много улыбок!'
        }
    ]
    return render_template("hello.html", title='Страница приветствия', user=user, posts=posts)


@app.route('/blank')
def blank():
    return render_template("blank.html")
